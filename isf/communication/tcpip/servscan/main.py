import nmap
from isf.core import logger
import nmap, masscan, os, sys
from . import conf
import requests
import time


class servScan:
    ips = ''
    ports = ''

    def __init__(self, ips='127.0.0.1', ports='1-1024'):
        self.ips = ips
        self.ports = ports

    def nmap_scan(self, service_detect=True, tcp=True, udp=False, fast=True,
                  dns_res=False, arp=False, ping=False, nice_output=True):
        '''
        :param service_detect: turn on service recognition
        :param tcp: Scan tcp ports
        :param udp: Scan udp ports
        :param fast: Increase speed of scanning
        :return:
        '''

        scan_arg_str = ''

        if tcp == udp == False:
            logger.error('TCP and UDP scan was canceled.')
            return
        elif tcp == udp == True:
            scan_arg_str = '-sU -sT '
        elif tcp:
            scan_arg_str = '-sT '
        elif udp:
            scan_arg_str = '-sU '

        if service_detect:
            scan_arg_str += '-sV '

        if fast:
            scan_arg_str += '-T5 '

        if dns_res:
            scan_arg_str += '-R '
        else:
            scan_arg_str += '-n '

        if arp:
            scan_arg_str += '-PR '
        else:
            scan_arg_str += '--disable-arp-ping '

        if not ping:
            scan_arg_str += '-Pn '
        logger.warning('Windows hosts scan slower than linux!!!')
        nmap_obj = nmap.PortScanner()

        logger.info(
            'Starting nmap scan with options "{}" !'.format(scan_arg_str))
        try:
            result = nmap_obj.scan(self.ips, self.ports, arguments=scan_arg_str)
        except:
            logger.error('Error while running nmap')
            return None
        logger.info('Scan completed!')
        logger.info('Scanned hosts: {}'.format(','.join(nmap_obj.all_hosts())))
        res_scan = result['scan']

        # removing unusable fields
        for ip in res_scan:
            del res_scan[ip]['hostnames']
            del res_scan[ip]['addresses']
            del res_scan[ip]['vendor']
            if not 'udp' in res_scan[ip]:
                res_scan[ip]['udp'] = {}
            if not 'tcp' in res_scan[ip]:
                res_scan[ip]['tcp'] = {}

        if nice_output:
            for ip in res_scan:
                logger.info('Host: {}'.format(ip))
                if len(res_scan[ip]['tcp']) > 0:
                    logger.info('\t\tTCP-ports:')
                    for tcp_port in res_scan[ip]['tcp']:
                        logger.info('\t\t\t{}'.format(tcp_port))
                        logger.info('\t\t\t\tState: {}'.format(
                            res_scan[ip]['tcp'][tcp_port]['state']))
                        logger.info('\t\t\t\tService name: {}'.format(
                            res_scan[ip]['tcp'][tcp_port]['name']))
                        if res_scan[ip]['tcp'][tcp_port]['version'] != '':
                            logger.info('\t\t\t\tService version: {}'.format(
                                res_scan[ip]['tcp'][tcp_port]['version']))
                        if res_scan[ip]['tcp'][tcp_port]['product'] != '':
                            logger.info('\t\t\t\tService product: {}'.format(
                                res_scan[ip]['tcp'][tcp_port]['product']))
                        if res_scan[ip]['tcp'][tcp_port]['extrainfo']:
                            logger.info('\t\t\t\tService info: {}'.format(
                                res_scan[ip]['tcp'][tcp_port]['extrainfo']))
                if len(res_scan[ip]['udp']) > 0:
                    logger.info('\t\tUDP-ports:')
                    for udp_port in res_scan[ip]['udp']:
                        logger.info('\t\t\t{}'.format(udp_port))
                        logger.info('\t\t\t\tState: {}'.format(
                            res_scan[ip]['udp'][udp_port]['state']))
                        logger.info('\t\t\t\tService name: {}'.format(
                            res_scan[ip]['udp'][udp_port]['name']))
                        if res_scan[ip]['udp'][udp_port]['version'] != '':
                            logger.info('\t\t\t\tService version: {}'.format(
                                res_scan[ip]['udp'][udp_port]['version']))
                        if res_scan[ip]['udp'][udp_port]['product'] != '':
                            logger.info('\t\t\t\tService product: {}'.format(
                                res_scan[ip]['udp'][udp_port]['product']))
                        if res_scan[ip]['udp'][udp_port]['extrainfo']:
                            logger.info('\t\t\t\tService info: {}'.format(
                                res_scan[ip]['udp'][udp_port]['extrainfo']))

        return res_scan

    def mass_install_windows(self):
        if os.path.isfile(conf.mass_win_path):
            return conf.mass_win_path
        logger.info('Installing Windows masscan')
        try:
            r = requests.get(conf.mass_win_url)
            f = open(conf.mass_win_path, 'wb')
            f.write(r.content)
            f.close()
        except:
            logger.error(
                "Can't save MassScan from {} to {}".format(conf.mass_win_url,
                                                           conf.mass_win_path))
            return None
        return conf.mass_win_path

    def mass_install_linux(self, bits=32):
        if bits == 32:
            if os.path.isfile(conf.mass_lin32_path):
                return
        if bits == 64:
            if os.path.isfile(conf.mass_lin64_path):
                return
        logger.error('You need to install masscan for linux in PATH')
        return None
        # TODO: autoinstall will be added later

    def mass_install_macos(self):
        logger.error('You need to install masscan for linux in PATH')
        return None

    def mass_scan(self, udp=False, banners=False,
                  max_rate=1000, ping=False, retries=1, nice_output=True):
        # TODO: optimize for mips/

        # preparation
        try:
            mas_obj = masscan.PortScanner()
        except masscan.masscan.PortScannerError:
            install_result = ''
            logger.warning(
                "Can't find masscan in PATH, will try to use it from resources folder (portable version)")
            if sys.platform.startswith('win'):
                install_result = self.mass_install_windows()
            elif sys.platform.startswith('linux'):
                install_result = self.mass_install_linux()
            elif sys.platform.startswith('darwin'):
                install_result = self.mass_install_macos()
            else:
                logger.error("Can't detect OS. Exiting..")
                return

            if install_result is None:
                return

        try:
            mas_obj = masscan.PortScanner(
                masscan_search_path=[conf.mass_win_path])
        except KeyError:  # masscan.masscan.PortScannerError:
            logger.error('Second masscan execute attempt was failed!')
            return

        # mas_obj must exist

        ports_formatted = self.ports
        if udp:
            ports_formatted = 'U:' + ports_formatted.replace(',', ',U:')

        scan_arg_str = ''
        scan_arg_str += ' --max-rate {}'.format(max_rate)
        scan_arg_str += ' --retries {}'.format(retries)
        if banners:
            scan_arg_str += ' --banners'
        if ping:
            scan_arg_str += ' --ping'


        logger.info('Masscan started with args: {}'.format(scan_arg_str))
        try:
            result = mas_obj.scan(self.ips, ports=ports_formatted,
                                  arguments=scan_arg_str)
            res_scan = result['scan']
        except:
            logger.error('Error while running masscan!')
            return None

        #delete unused
        for ip in res_scan:
            if 'tcp' in res_scan[ip]:
                for port in res_scan[ip]['tcp']:
                    del res_scan[ip]['tcp'][port]['reason']
                    del res_scan[ip]['tcp'][port]['reason_ttl']
                    del res_scan[ip]['tcp'][port]['endtime']
            else:
                res_scan[ip]['tcp'] = {}
            if 'udp' in res_scan[ip]:
                for port in res_scan[ip]['udp']:
                    del res_scan[ip]['udp'][port]['reason']
                    del res_scan[ip]['udp'][port]['reason_ttl']
                    del res_scan[ip]['udp'][port]['endtime']
            else:
                res_scan[ip]['udp'] = {}
            if 'icmp' in res_scan[ip]:
                for port in res_scan[ip]['icmp']:
                    del res_scan[ip]['icmp'][port]['reason']
                    del res_scan[ip]['icmp'][port]['reason_ttl']
                    del res_scan[ip]['icmp'][port]['endtime']
            else:
                res_scan[ip]['icmp'] = {}

        if nice_output:
            for ip in res_scan:
                logger.info('Host: {}'.format(ip))
                if len(res_scan[ip]['tcp']) > 0:
                    logger.info('\t\tTCP-ports:')
                    for tcp_port in res_scan[ip]['tcp']:
                        logger.info('\t\t\t{}'.format(tcp_port))
                        logger.info('\t\t\t\tState: {}'.format(
                            res_scan[ip]['tcp'][tcp_port]['state']))
                        if res_scan[ip]['tcp'][tcp_port]['services'] is not []:
                            logger.info('\t\t\t\tServices: {}'.format(
                                str(res_scan[ip]['tcp'][tcp_port]['services'])))

                if len(res_scan[ip]['udp']) > 0:
                    logger.info('\t\tUDP-ports:')
                    for udp_port in res_scan[ip]['udp']:
                        logger.info('\t\t\t{}'.format(udp_port))
                        logger.info('\t\t\t\tState: {}'.format(
                            res_scan[ip]['udp'][udp_port]['state']))
                        if res_scan[ip]['udp'][udp_port]['services'] is not []:
                            logger.info('\t\t\t\tServices: {}'.format(
                                str(res_scan[ip]['udp'][udp_port]['services'])))
                if len(res_scan[ip]['icmp']) > 0:
                    logger.info('\t\tICMP:')
                    for icmp_port in res_scan[ip]['icmp']:
                        logger.info('\t\t\t{}'.format(icmp_port))
                        logger.info('\t\t\t\tState: {}'.format(
                            res_scan[ip]['icmp'][icmp_port]['state']))
                        if res_scan[ip]['icmp'][icmp_port]['services'] is not []:
                            logger.info('\t\t\t\tServices: {}'.format(
                                str(res_scan[ip]['icmp'][icmp_port]['services'])))

        return res_scan
