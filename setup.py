from setuptools import setup, find_namespace_packages

setup(
    name='communication.tcpip.servscan',
    version='0.1.0',
    description='Port scanner and service detection.',
    author='Invuls',
    packages=find_namespace_packages(),
    package_data={'isf.communication.tcpip.servscan.resources': ['*', '**/*']},
    zip_safe=False
)
